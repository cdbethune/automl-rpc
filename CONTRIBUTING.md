# Contributing to the D3M AutoML API

## Change proposals

Suggested changes to the API can be made in one of two ways:

1.  By logging issues against the repository.
2.  Through merge requests.

Discussion of high level changes can be raised with the group and commented on using GitLab's issue tracking system.
Concrete changes can be proposed by modifying code and submitting a merge request for review.
Merge requests should be marked `WIP` in their title if they are being actively updated, with the tag being removed when the code is ready for final review.
All merge requests have to be against a `devel` branch.
Merge requests should include also an entry to `HISTORY.md` with a summary of a change.

Feedback from users of the API on ideas and code is very valuable. 
Voting on issues and merge requests is the simplest form of feedback, and quite useful. 
A vote for an issue is interpreted to mean support for the proposed direction or change.
A vote for a merge request is interpreted to mean a sign off on the code implementation as ready and appropriate for merging.

## Change philosophy

_The underlying goal should be to allow everyone to pursue their own research trajectories, without imposing undue burdens on others._

The balance between freedom to add desired features to the core, and imposing burden on other systems can be a difficult compromise and typically includes considerations such as:
* how much work is required to support a feature,
* how much the feature forces or constrains internal design on the API or other systems,
* the ease with which performers can opt out (e.g. not support a value type), so as to support gradual implementation and accommodate different performer priorities,
* how many systems presently could foresee using such a feature,
* the existence or lack of alternative approaches to achieve the same desired functionality,
* work balance between AutoML systems and clients for adoption,
* as well as whether a feature is judged feasible.

Due to the nature of research, these opinions can change over time, so feel free to revisit proposed functionality changes over time if an unmet need persists.

## Style

1. All changes to the `.proto` files should follow Google's [Protobuf Style Guide](https://developers.google.com/protocol-buffers/docs/style).
1. Indentation should be 4 spaces.
1. Proto file names should use `snake_case`, with extensions ending in `_ext`.  Example: `dataflow_ext.proto` 
